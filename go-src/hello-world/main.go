package main
import (
    "github.com/aws/aws-lambda-go/events"
    "github.com/aws/aws-lambda-go/lambda"
    "github.com/honeycombio/libhoney-go"
    "github.com/honeycombio/libhoney-go/transmission"
    //"bytes"
    //"encoding/json"
    "fmt"
    "io/ioutil"
    "log"
    "net/http"
)

func handler(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
    // Create an event, add some data

    getVindsidenReport(1)


    ev := libhoney.NewEvent()
    ev.Add(map[string]interface{}{
        "method":       request.HTTPMethod,
        "hostname":     request.Resource,
        "request_path": request.Path,
        "name":         "devtips",
    })
    // This event will be sent regardless of how we exit
    defer ev.Send()
    ev.AddField("status_code", 200)
    return &events.APIGatewayProxyResponse{
        StatusCode: 200,
        Body:       "Hello, World",
    }, nil
}

func main() {
    libhoney.Init(libhoney.Config{
        // APIKey: "",
        Dataset:      "netlify-lambdas",
        Transmission: &transmission.WriterSender{},
    })
    // Flush any pending calls to Honeycomb before exiting
    defer libhoney.Close()
    // Make the handler available for Remote Procedure Call by AWS Lambda
    lambda.Start(handler)
}

func getVindsidenReport(location int) {
    get()
}

func get() {
    fmt.Println("1. Performing Http Get...")
    resp, err := http.Get("http://vindsiden.no/xml.aspx?id=3&callback=?")
    if err != nil {
        log.Fatalln(err)
    }

    defer resp.Body.Close()
    bodyBytes, _ := ioutil.ReadAll(resp.Body)

    // Convert response body to string
    bodyString := string(bodyBytes)
    fmt.Println("API Response as String:\n" + bodyString)

    // Convert response body to Todo struct
    //var todoStruct Todo
    //json.Unmarshal(bodyBytes, &todoStruct)
    //fmt.Printf("API Response as struct %+v\n", todoStruct)
}