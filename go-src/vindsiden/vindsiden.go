package main

import (
	"encoding/xml"
	"encoding/json"
    "fmt"
    "io/ioutil"
    "log"
	"net/http"
	"strconv"
)

// Vindsiden struct
type Vindsiden struct {
	Measurements []struct {
		DataID    			int    		`xml:"DataID"`
		StationID   		int    		`xml:"StationID"`
		Time     			string 		`xml:"Time"`
		WindAvg 			float32   	`xml:"WindAvg"`
		WindVectorAvg 		int   		`xml:"WindVectorAvg"`
		WindStDev 			float32   	`xml:"WindStDev"`
		WindMax 			float32   	`xml:"WindMax"`
		WindMin 			float32   	`xml:"WindMin"`
		DirectionAvg 		int		   	`xml:"DirectionAvg"`
		DirectionVectorAvg 	int		   	`xml:"DirectionVectorAvg"`
		DirectionStDev 		float32   	`xml:"DirectionStDev"`
	} `xml:"Measurement"`
}

type VindsidenOutput struct {
	Time					[]string
	WindAvg					[]float32
	WindMax					[]float32
	WindMin					[]float32
	DirectionAvg			[]int
}

func vindsiden_xml_http_get(id int) string {
	fmt.Println("Performing Http Get From Vindsiden")

	// Do http get. Result is in XML
    resp, err := http.Get("http://vindsiden.no/xml.aspx?id=" + strconv.Itoa(id) + "&callback=?")
    if err != nil {
        log.Fatalln(err)
    }

    defer resp.Body.Close()
    bodyBytes, _ := ioutil.ReadAll(resp.Body)

	// Unmarshall xml to struct
    var vindsidenStruct Vindsiden
	xml.Unmarshal(bodyBytes, &vindsidenStruct)

	entries := len(vindsidenStruct.Measurements)

	time := make([]string, entries)
	windAvg := make([]float32, entries)
	windMax := make([]float32, entries)
	windMin := make([]float32, entries)
	directionAvg := make([]int, entries)

	fmt.Print("Restructuring to preferred format. Number of entries: ")
	fmt.Println(entries)

	for i, entry := range vindsidenStruct.Measurements {
		time[i] = entry.Time
		windAvg[i] = entry.WindAvg
		windMax[i] = entry.WindMax
		windMin[i] = entry.WindMin
		directionAvg[i] = entry.DirectionAvg
	}

	vindsidenOutput := VindsidenOutput{time, windAvg, windMax, windMin, directionAvg}

	// Convert it to json, then return the json string
	fmt.Println("Converting to json")
	json, _ := json.Marshal(vindsidenOutput)

	return string(json)
}
