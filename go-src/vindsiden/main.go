package main
import (
    "github.com/aws/aws-lambda-go/events"
    "github.com/aws/aws-lambda-go/lambda"
    "github.com/honeycombio/libhoney-go"
    "github.com/honeycombio/libhoney-go/transmission"
)

func handler(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
    // Decode input
    ev := libhoney.NewEvent()
    ev.Add(map[string]interface{}{
        "method":       request.HTTPMethod,
        "hostname":     request.Resource,
        "request_path": request.Path,
        "name":         "devtips",
    })

    // Do actions
    vindReport := vindsiden_xml_http_get(9)

    // Reply to caller
    defer ev.Send()
    ev.AddField("status_code", 200)
    return &events.APIGatewayProxyResponse{
        StatusCode: 200,
        Body: vindReport,
    }, nil
}

func main() {
    libhoney.Init(libhoney.Config{
        // APIKey: "",
        Dataset:      "netlify-lambdas",
        Transmission: &transmission.WriterSender{},
    })
    // Flush any pending calls to Honeycomb before exiting
    defer libhoney.Close()
    // Make the handler available for Remote Procedure Call by AWS Lambda
    lambda.Start(handler)
}
