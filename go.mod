module gitlab.com/frikksol/website

go 1.14

require (
	github.com/aws/aws-lambda-go v1.16.0
	github.com/honeycombio/libhoney-go v1.12.4
)
